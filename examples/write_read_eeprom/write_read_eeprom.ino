#include "ds_external_eeprom_i2c.h"

// for AT24C32, address 0x50
ds_external_eeprom_i2c ext_eeprom(32,16,0x50);

// for AT24C04, address 0x50 (default)
// ds_external_eeprom_i2c ext_eeprom(4);

void setup(){
    Serial.begin(9600);

    Serial.print("Check EXT_EEPROM:  ");
    Serial.println(ext_eeprom.check() ? "OK":"FALSE");

    Serial.println("Write byte to EEPROM memory...");
    ext_eeprom.writeByte(0,0xAA);

    Serial.print("Read byte from EEPROM memory... ");
    Serial.println(ext_eeprom.readByte(0), HEX);

    Serial.println("\n\n");

    // Declare byte arrays.
    byte inputBytes[100] = { 0 };
    byte outputBytes[100] = { 0 };

    // Fill input array. 
    for (byte i = 0; i < 32; i++) inputBytes[i] = i;
    
    // Write input array to EEPROM memory.
    Serial.println("Write bytes to EEPROM memory...");
    ext_eeprom.writeBytes(0x00, 32, inputBytes);
	
    // Read array with bytes read from EEPROM memory.
    Serial.println("Read bytes from EEPROM memory...");
    ext_eeprom.readBytes(0x00, 32, outputBytes);

    for (byte i = 0; i < 32; i++){
        Serial.print(outputBytes[i], HEX);	Serial.print(" ");
    }
}

void loop(){

}


