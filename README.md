# ds_external_eeprom_i2c

EEPROM 24C04 - 24C512 library for Arduino

Универсальная библиотека для работы с внешней EEPROM по I2C протоколу (24XX02 - 24XX512)

Чтение/запись одного байта или массива байт.

Работа с микросхемами такие как 24C04, 24C16, 24C32 и т.д.

# Functions
```C++
bool check();

// read/write byte
byte readByte(int address);
void writeByte(int address, byte data);

// read/write bytes
void readBytes(int address, int length, byte* p_buffer);
void writeBytes(int address, int length, byte* p_data);
```

# Examples
* [write_read_eeprom.ino](https://bitbucket.org/admsasha/ds_external_eeprom_i2c/src/master/examples/write_read_eeprom/write_read_eeprom.ino)
